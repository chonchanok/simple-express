const express = require('express')
const requestIp = require('request-ip')
require('console-stamp')( console, 'yyyy/mm/dd HH:MM:ss.l' );

const app = express()
const port = process.env.PORT || 3000

app.use(requestIp.mw())


app.get('/', (req, res) => {
  console.info(`${req.url} ${req.clientIp}`)
  res.json({ status: 'success', message: 'Hello World!' })
})

app.listen(port, () => {
  console.info(`Listening on port ${port}`)
})